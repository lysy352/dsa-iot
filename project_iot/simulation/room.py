import time

from project_iot.model.curtain_controller import CurtainController
from project_iot.model.luminance_sensor import LuminanceSensor
from project_iot.model.light_controller import LightController
from project_iot.model.temperature_sensor import TemperatureSensor


class Room:
    def __init__(self, name, temp, sun):
        self.name = name
        self.sun = sun

        self.temperatureSensor = TemperatureSensor(name + "temperature")
        self.temperatureSensor.temperature = temp

        self.luminance_sensor = LuminanceSensor(name + "luminance")
        self.luminance_sensor.luminance_level = sun

        self.curtain_controller = CurtainController(name + "curtain")
        self.curtain_controller.curtain_level = 0

        self.light_controller = LightController(name + 'light')
        self.light_controller.on = False

    def move_curtain(self, level):
        if level > 100:
            level = 100
        if level < 0:
            level = 0

        direction = -1 if level < self.curtain_controller.curtain_level else 1

        while self.curtain_controller.curtain_level != level:
            self.curtain_controller.curtain_level += direction
            self.luminance_sensor.luminance_level += -direction
            if self.luminance_sensor.luminance_level % 20 == 0:
                self.temperatureSensor.temperature += -direction
            time.sleep(0.1)

    def get_temperature(self):
        return self.temperatureSensor.temperature

    def get_luminance(self):
        return self.luminance_sensor.luminance_level

    def is_light_on(self):
        return self.light_controller.is_on()

    def switch_light(self):
        print('Light: {}'.format(self.light_controller.is_on()))
        self.light_controller.switch_light()
        self.luminance_sensor.luminance_level += 300 if self.light_controller.is_on() else -300

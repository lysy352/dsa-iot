from dslink import DSLink, Configuration, Node

from project_iot.simulation.room import Room


class CurtainDSLink(DSLink):
    def __init__(self, room):
        self.room = room
        DSLink.__init__(self, Configuration(room.curtain_controller.name, responder=True))

    def start(self):
        print('Starting CurtainDSLink')
        self.responder.profile_manager.create_profile("move_curtain")
        self.responder.profile_manager.register_callback("move_curtain", self.move_curtain_action)
		self.call_later(1, self.update)

    def get_default_nodes(self, super_root):
        my_node = Node('CurtainLevel', super_root)
        my_node.set_display_name('Curtain level')
        my_node.set_type('int')
        my_node.set_value(self.room.curtain_controller.curtain_level)

        move_curtain_node = Node("MoveCurtain", super_root)
        move_curtain_node.set_display_name("move_curtain")
        move_curtain_node.set_profile("move_curtain")
        move_curtain_node.set_config("$is", "move_curtain")
        move_curtain_node.set_invokable("write")

        move_curtain_node.set_parameters([
            {
                "name": "Level",
                "type": "int"
            }
        ])

        super_root.add_child(my_node)
        super_root.add_child(move_curtain_node)

        return super_root

    def move_curtain_action(self, parameters):
        print("action")
        level = int(parameters.params["Level"])
        print(str(level))
        self.room.move_curtain(level)
        return [[]]

    def update(self):
        my_node = self.responder.get_super_root().get('/CurtainLevel')
        my_node.set_value(self.room.curtain_controller.curtain_level)

        self.call_later(1, self.update)


if __name__ == '__main__':
    CurtainDSLink(Room('living room', 20, 200))

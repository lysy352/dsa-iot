from dslink import DSLink, Configuration, Node

from project_iot.simulation.room import Room


class SwitchDSLink(DSLink):
    def __init__(self, room):
        self.room = room
        DSLink.__init__(self, Configuration(room.light_switch.name, responder=True))

    def get_default_nodes(self, super_root):
        my_node = Node('LightSwitch', super_root)
        my_node.set_display_name('Light switch on')
        my_node.set_type('bool')
        my_node.set_value(self.room.light_switch.on)

        super_root.add_child(my_node)
        return super_root

    def start(self):
        self.call_later(1, self.update)

    def update(self):
        my_node = self.responder.get_super_root().get('/LightSwitch')
        my_node.set_value(self.room.light_switch.on)

        self.call_later(1, self.update)


if __name__ == '__main__':
    SwitchDSLink(Room('living room', 20, 200))

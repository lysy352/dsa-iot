from dslink import DSLink, Configuration, Node
from threading import Thread
from project_iot.simulation.room import Room


class RoomDSLink(DSLink):
    def __init__(self, room):
        self.room = room
        DSLink.__init__(self, Configuration(room.name, responder=True, requester=True))

    def start(self):
        self.responder.profile_manager.create_profile("move_curtain")
        self.responder.profile_manager.register_callback("move_curtain", self.move_curtain)
        self.responder.profile_manager.create_profile("switch_light")
        self.responder.profile_manager.register_callback("switch_light", self.switch_light)
        self.call_later(1, self.update)

    def get_default_nodes(self, super_root):
        luminance_node = Node('luminance_sensor', super_root)
        luminance_node.set_display_name('luminance_sensor')
        luminance_node.set_type('int')
        luminance_node.set_value(self.room.luminance_sensor.luminance_level)
        super_root.add_child(luminance_node)

        temp_node = Node('temperature_sensor', super_root)
        temp_node.set_display_name('temperature_sensor')
        temp_node.set_type('int')
        temp_node.set_value(self.room.temperatureSensor.temperature)
        super_root.add_child(temp_node)

        curtain_node = Node('curtain_controller', super_root)
        curtain_node.set_display_name('curtain_controller')
        curtain_node.set_type('int')
        curtain_node.set_value(self.room.curtain_controller.curtain_level)
        super_root.add_child(curtain_node)

        light_node = Node('light_controller', super_root)
        light_node.set_display_name('light_controller')
        light_node.set_type('bool')
        light_node.set_value(self.room.light_controller.is_on())
        super_root.add_child(light_node)

        move_curtain_node = Node("move_curtain", super_root)
        move_curtain_node.set_display_name("move_curtain")
        move_curtain_node.set_config("$is", "move_curtain")
        move_curtain_node.set_invokable("config")
        move_curtain_node.set_parameters([
            {
                "name": "Level",
                "type": "int"
            }
        ])
        move_curtain_node.set_columns([
            {
                "name": "Success",
                "type": "bool"
            }
        ])
        super_root.add_child(move_curtain_node)

        switch_light_node = Node("switch_light", super_root)
        switch_light_node.set_display_name("switch_light")
        switch_light_node.set_config("$is", "switch_light")
        switch_light_node.set_invokable("config")
        switch_light_node.set_columns([
            {
                "name": "Success",
                "type": "bool"
            }
        ])
        super_root.add_child(switch_light_node)

        return super_root

    def update(self):
        self.responder.get_super_root().get('/luminance_sensor').set_value(self.room.luminance_sensor.luminance_level)
        self.responder.get_super_root().get('/temperature_sensor').set_value(self.room.temperatureSensor.temperature)
        self.responder.get_super_root().get('/curtain_controller').set_value(self.room.curtain_controller.curtain_level)
        self.responder.get_super_root().get('/light_controller').set_value(self.room.light_controller.is_on())

        self.call_later(1, self.update)

    def move_curtain(self, parameters):
        level = int(parameters[1]["Level"])
        t = Thread(target=lambda: self.room.move_curtain(level))
        t.start()
        return [[True]]

    def switch_light(self, parameters):
        self.room.switch_light()
        return [[True]]


if __name__ == '__main__':
    RoomDSLink(Room('living_room', 22, 300))

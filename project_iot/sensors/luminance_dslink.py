from dslink import DSLink, Configuration, Node

from project_iot.simulation.room import Room


class LuminanceDSLink(DSLink):
    def __init__(self, room):
        self.room = room
        DSLink.__init__(self, Configuration(room.luminance_sensor.name, responder=True))

    def get_default_nodes(self, super_root):
        my_node = Node('LuminanceSensor', super_root)
        my_node.set_display_name('Luminance sensor')
        my_node.set_type('int')
        my_node.set_value(self.room.luminance_sensor.luminance_level)

        super_root.add_child(my_node)
        return super_root

    def start(self):
        self.call_later(1, self.update)

    def update(self):
        my_node = self.responder.get_super_root().get('/LuminanceSensor')
        my_node.set_value(self.room.luminance_sensor.luminance_level)

        self.call_later(1, self.update)


if __name__ == '__main__':
    LuminanceDSLink(Room('living room', 20, 200))

from dslink import DSLink, Configuration, Node

from project_iot.simulation.room import Room


class TemperatureDSLink(DSLink):
    def __init__(self, room):
        self.room = room
        DSLink.__init__(self, Configuration(room.temperatureSensor.name, responder=True))

    def get_default_nodes(self, super_root):
        my_node = Node('TemperatureSensor', super_root)
        my_node.set_display_name('Temperature sensor')
        my_node.set_type('int')
        my_node.set_value(self.room.temperature_sensor.get_temperature())

        super_root.add_child(my_node)
        return super_root

    def start(self):
        self.call_later(1, self.update)

    def update(self):
        my_node = self.responder.get_super_root().get('/TemperatureSensor')
        my_node.set_value(self.room.temperature_sensor.temperature)

        self.call_later(1, self.update)


if __name__ == '__main__':
    TemperatureDSLink(Room('living room', 20, 200))

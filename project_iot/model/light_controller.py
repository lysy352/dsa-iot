class LightController:
    def __init__(self, name):
        self.name = name
        self.on = False

    def switch_light(self):
        self.on = not self.on

    def is_on(self):
        return self.on

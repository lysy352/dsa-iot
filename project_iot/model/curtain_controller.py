import time


class CurtainController:
    MIN_CURTAIN_LEVEL = 0
    MAX_CURTAIN_LEVEL = 100
    STEP = 1

    def __init__(self, name):
        self.name = name
        self.curtain_level = 0

    def move(self, level):
        direction = -1 if level < self.curtain_level else 1
        while self.curtain_level != level and \
                self.curtain_level <= self.MAX_CURTAIN_LEVEL and \
                self.curtain_level >= self.MIN_CURTAIN_LEVEL:

            self.curtain_level += direction * self.STEP
            time.sleep(0.1)
